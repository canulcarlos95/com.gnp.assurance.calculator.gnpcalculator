jQuery(function(){
    var i = new Intl.NumberFormat('en-US', { 
                    style: 'currency', 
                    currency: 'USD' 
        });

    function getQuotation(params){

        $.ajax({
           url: "/bin/get/quotation?"+params,
           type: 'get',
           beforeSend: function(){
            // Show image container
            $("#loader").show();
           },
           success: function(data){
                if(data && data!=""){
                    fillTable(data);
                }else{
                    $('#errorModal').show();
                }
           },
           complete:function(data){
            // Hide image container
            $('html,body').animate({
                scrollTop: $("#tablaCoti").offset().top
            }, 'slow');
            $("#loader").hide();
           }
         });
    }

    function fillTable(quotation){
        var info = JSON.parse(quotation);
		$('#tablaCoti').show();
        $.each(info,function(){
            if(this.coberturas!=undefined){
                $('#titulos').append($("<td />").text(this.tipopaquete));
                $('#primasTotales').append($("<td />").text(i.format(this.totales.totalpagar)));
                fillCoberturas(this.coberturas,this.tipopaquete);
            }
        });
    }

    function fillCoberturas(coberturas,tipopaquete){
        $.each(coberturas,function(){
            switch (this.clave) {
              case "0000001289":
                    $("#0000001289").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
              case "0000001288":
                   	$("#0000001288").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
              case "0000000891":
                    $("#0000000891").append($("<td/>").html(this.suma + "<br> deducible: "+this.deducible));
                break;
              case "0000000916":
                    $("#0000000916").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
			  case "0000001273":
                    $("#0000001273").append($("<td/>").html(i.format(this.suma) + "<br> deducible: No Aplica"));
                break;
              case "0000001285":
                    $("#0000001285").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
              case "0000000906":
                    $("#0000000906").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
              case "0000000904":
                    $("#0000000904").append($("<td/>").html(i.format(this.suma) + "<br> deducible: "+this.deducible));
                break;
              case "0000001268":
                    $("#0000001268").append($("<td/>").html(this.suma + "<br>deducible: "+this.deducible));
                break;
           }
        });
        if(tipopaquete != "AMPLIA"){
			$("#0000001288").append($("<td/>").html("No Aplica <br>deducible: No Aplica"));
            $("#0000001289").append($("<td/>").html("No Aplica <br>deducible: No Aplica"));
            $("#0000000891").append($("<td/>").html("No Aplica <br>deducible: No Aplica"));
        }
        if(tipopaquete == "RESPONSABILIDAD CIVIL"){
            $("#0000000916").append($("<td/>").html("No Aplica <br> deducible: No Aplica"));
        }
    }

	$('#btnCoizar').click(function(){
		var params = $('#requestQuotation').serialize();

        getQuotation(params);

    });

	$('#closeModal').click(function(){$('#errorModal').hide()});

});