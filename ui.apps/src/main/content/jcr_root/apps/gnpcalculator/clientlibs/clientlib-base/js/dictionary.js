jQuery(function(){

	var vType ="AUT";

    function getAPI(){
        $.ajax({
            url: "/content/dam/gnpcalculator/GNP_JSON.json",
            method: 'GET',
            type: 'JSON',
        }).done(function(data) {
            setAPI(data);
        });
    }

	var setAPI = function(apiData){
        window.APIInfo = apiData;
        genericRequest(APIInfo.segments.years.url,APIInfo.segments.years.param,vType,"years");
    }

    function genericRequest(segment,param,key,segmentType){
        $.ajax({
            url: APIInfo.url.api_path+segment+"?"+param+"="+key,
            method: 'GET',
            type: 'JSON',
            beforeSend: function(){
            	// Show image container
            	$("#loader").show();
            },
            success: function(data){
                if(segmentType == "years"){
                    years = data;
                    fillOptions(years,"selectorModelo");
                }
                if(segmentType == "brands"){
                    brands = data;
                    fillOptions(brands,"selectorMarca");
                }
                if(segmentType == "sub_brand"){
                    brands = data;
                    fillOptions(brands,"selectorSubMarca");
                }
                if(segmentType == "versions"){
                    brands = data;
                    fillOptions(brands,"selectorDescripcion");
                }
            },
            complete:function(data){
            	// Hide image container
                $("#loader").hide();
           	}
        });
    }

    function fillOptions(option,selector){
       $.each(option.items,function(){
           if(selector!="selectorModelo"){
				$('#'+selector).append($("<option />").val(this.clave).text(this.nombre));
           }else{
           		$('#'+selector).append($("<option />").val(this).text(this));
           }
       });
    }

    $('#selectorTipo').change(function(){
		vType = $(this).children("option:selected").val();
		$('.clear').empty().append('<option selected value="">Selecciona Opción</option>');
		genericRequest(APIInfo.segments.years.url,APIInfo.segments.years.param,vType,"years");
    });

    $('#selectorModelo').change(function(){
        $('.selectorMarca,.selectorSubMarca,.selectorDescripcion').empty().append('<option selected value="">Selecciona Opción</option>');
		selectedYear = $(this).children("option:selected").val();
		var url = APIInfo.segments.brands.url.replace("$year",selectedYear);
		genericRequest(url,APIInfo.segments.brands.param,vType,"brands");
    });

    $('#selectorMarca').change(function(){
        $('.selectorSubMarca,.selectorDescripcion').empty().append('<option selected value="">Selecciona Opción</option>');
		window.selectedBrand = $(this).children("option:selected").val();
		$('#marca').val($(this).children("option:selected").text());
		var url = APIInfo.segments.sub_brand.url.replace("$year",selectedYear).replace("$brand",selectedBrand);
		genericRequest(url,APIInfo.segments.sub_brand.param,vType,"sub_brand");
    });

    $('#selectorSubMarca').change(function(){
        $('.selectorDescripcion').empty().append('<option selected value="">Selecciona Opción</option>');
		selectedSubBrand = $(this).children("option:selected").val();
        $('#submarca').val($(this).children("option:selected").text());
		var url = APIInfo.segments.versions.url.replace("$year",selectedYear).replace("$brand",selectedBrand).replace("$sub_brand",selectedSubBrand);
		genericRequest(url,APIInfo.segments.versions.param,vType,"versions");
    });

    $('#selectorDescripcion').change(function(){
        $('#descripcion').val($(this).children("option:selected").text());
    });

    $( document ).ready(function() {
        var vType = "AUT";
        getAPI();
	});


});