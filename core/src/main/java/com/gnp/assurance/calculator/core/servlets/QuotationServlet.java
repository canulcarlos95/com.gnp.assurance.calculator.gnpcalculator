package com.gnp.assurance.calculator.core.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(service=Servlet.class,
			property={
			    Constants.SERVICE_DESCRIPTION + "= Quotation Servlet",
			    "sling.servlet.methods=" + HttpConstants.METHOD_GET,
			    "sling.servlet.paths="+ "/bin/get/quotation"
			})
public class QuotationServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = LoggerFactory.getLogger(QuotationServlet.class);

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		logger.info("###### Inside Servlet #####");
		
		Map<String, String[]> params = request.getParameterMap();
		Map<String, String[]> modifiable = addParams(params,request);
		
		ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(modifiable).replace("}", "");
            json = json + ",\"tos\":false,\"deseoDeContacto\":false }";
            String postResponse = getRequest(json);

            response.setContentType("text/plain");
            response.getWriter().write(postResponse);
            response.getWriter().close();
            
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
	}

	public static String[] getParamsUrl(SlingHttpServletRequest requestInfo) {
		String[] params = new String[]{"www.gnp.com.mx/comparativa/#/cotizar?"
				+ "Anio="+requestInfo.getParameter("modelo")
				+ "&AutorizacionDatos=No"
				+ "&CadenaAvisoPrivacidad=No"
				+ "&CadenaDescripcionVehiculo="+requestInfo.getParameter("descripcion")
				+ "&CadenaMarca="+requestInfo.getParameter("marca")
				+ "&CadenaSubMarca="+requestInfo.getParameter("submarca")
				+ "&CodigoPostal="+requestInfo.getParameter("cp")
				+ "&CodigoPromocion="
				+ "&CorreoElectronico=" + requestInfo.getParameter("mail")
				+ "&DescripcionTipoTelefono="+requestInfo.getParameter("tipotelefono")
				+ "&DescripcionVehiculo="+requestInfo.getParameter("tipovehiculo")+":"
										 +requestInfo.getParameter("clavemarca")+":"
										 +requestInfo.getParameter("clavesubmarca")+":"
										 +requestInfo.getParameter("clavedescripcion")+":"
										 +requestInfo.getParameter("modelo")
				+ "&Edad="+requestInfo.getParameter("edad")
				+ "&Genero="+requestInfo.getParameter("sexo")
				+ "&Marca="+requestInfo.getParameter("clavemarca")
				+ "&SubMarca="+requestInfo.getParameter("clavesubmarca")
				+ "&Telefono="+requestInfo.getParameter("telefono")
				+ "&TipoTelefono=TLCL"
				+ "&TipoVehiculo="+requestInfo.getParameter("tipovehiculo")
				+ "&clientId=56112123.1581524149"
		};
		
		return params;
	}
	
	public static Map<String,String[]> addParams(Map<String, String[]> data,SlingHttpServletRequest req){
		Map<String, String[]> newMap = new HashMap<>(data);
		
		newMap.put("formapago",new String[]{"CL"});
		newMap.put("sitio",new String[]{"1"});
		newMap.put("numerocliente",new String[]{"1705089"});
		newMap.put("tipoCotizacion",new String[]{"0"});
		newMap.put("codigopromocion",new String[]{"COP0000332"});
		newMap.put("periodicidad",new String[]{"A"});
		newMap.put("clientId",new String[]{"56112123.1581524149"});
		newMap.put("fuente",new String[]{"PAGADO"});
		newMap.put("fechanac",new String[]{"19960212"});
		newMap.put("iniciovig",new String[]{"20200212"});
		newMap.put("finvig",new String[]{"20210212"});
		newMap.put("params",getParamsUrl(req));
		
		return newMap;
	}
	
	public static String getRequest(String requestBody) throws IOException{
		try {
			URL url = new URL("https://ventadirecta-api-dot-gnp-vdi-pro.appspot.com/_ah/api/gnp/v1/cotizar/cotizar");
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); 
			http.setDoOutput(true);
			
			byte[] out = requestBody.getBytes(StandardCharsets.UTF_8);
			int length = out.length;

			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			OutputStream os = http.getOutputStream();
			
			os.write(out);
			os.flush();
			os.close();
			
			InputStream responseStream = 
					new BufferedInputStream(http.getInputStream());

			BufferedReader responseStreamReader = 
					new BufferedReader(new InputStreamReader(responseStream));

			String line = "";
			StringBuilder stringBuilder = new StringBuilder();

			while ((line = responseStreamReader.readLine()) != null) {
			    stringBuilder.append(line).append("\n");
			}
			responseStreamReader.close();
			String response = stringBuilder.toString();
			
			logger.info("####Success#####");
			
			responseStream.close();
			
			http.disconnect();
			
			return response;
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		return "Error";
	}
	
}